**Project plan and implementation strategy:**
*  Milestones:
    * Reliable point to point one direction communication via the Digi Xbee Pro DigiMesh 2.4 RF Module. Deliverables: running xbee_sender and xbee_receiver applications plus their unit tests suite
    * Reliable bidirectional communication via the Digi Xbee Pro DigiMesh 2.4 RF Module. Deliverables: at least two running instances of the xbee_sender_receiver application plus its unit tests suite
    * Integration of DDS. Identification and implementation of the best DDS integration approach
    * Development of a test application built on top of DDS and able to communicate through the xbee mesh network. 
    * Deployment over a n number of instances. Deliverables: at least four running instances (nodes) of the xbee_dds_communicator application plus its unit tests suite. The test suite must include proof of resiliency against nodes failure


**Technical design and implementation plan :**

Wireless mesh networks (WMN) constitute the best solution to enable communication between small formations of cubesats since they can be rapidly deployed, are resilient, and readily expandable. They can be built upon low-cost commodity, off-the-shelf (COTS) components with support for a level of reconfigurability lacking in typically point-to-point or star topologies. The ability for each node in a mesh network to automatically determine neighbors enables the traffic to be diverted from failing nodes. 
Among the many Protocol/Devices that implement wireless mesh network topologies the Xbee Pro DigiMesh is proposed (Digi Xbee Pro DigiMesh 2.4 RF Module). 
This particular module fully implements the mesh concept and removes the single point of failure associated with coordinator or router nodes used by other protocols. In literature are available some examples of the use of this module for inter-spacecraft communication [1].
The overall network infrastructures (and related middleware at application level) will also need to provide the following properties: reliability, scalability, extensibility, fault-tolerance and performance. Fault-tolerance capabilities must provide full support for failover and recovery and efficient resource-aware redeployment and reconfiguration. Several application level specialized protocols (Communication Middlewares), such as Message Queuing Telemetry Transport (MQTT), Extensible Messaging and Presence Protocol (XMPP), Advanced Message Queueing Protocol (AMQP) and Data-Distribution Service (DDS) are available and could be used. Among those identified protocols, the DDS is the one which has been designed specifically to address the needs of mission and business-critical applications such as air traffic control, military applications, transportation management, healthcare, energy and utilities. DDS seems the best positioned protocol to support the foreseen application. DDS is supported by the Object Management Group (OMG) technology standards consortium and one of the most interesting features of the DDS protocol is the availability of quality of service (QoS) policies that allows optimization of the available network resources. QoS policies provide the benefits of controlling communication efficiency, determinism, and fault-tolerance. 
The main outcome of this project will be then the integration between the Xbee Pro DigiMesh Link layer protocol and the DDS application layer protocol as a foundation component for the inter satellites communication. 
All the development will be performed in Python on top of the existing digimesh [2] library and the rticonnextdds [3] open source DDS implementation from RTI.


**Wireframes:**

Two Digi Xbee Pro DigiMesh 2.4 RF Modules are already available and can be used to create an initial test setup such the one in figure.
Two additional modules will be procured for the final test setup. 
![figure](images/wireframe.jpg)


**References:**

* [1] Mesh Network Architecture for Enabling Inter-Spacecraft Communication https://digitalcommons.usu.edu/cgi/viewcontent.cgi?article=3804&context=smallsat
* [2] https://github.com/thom-nic/python-xbee/blob/master/xbee/digimesh.py
* [3] https://pypi.org/project/rticonnextdds-connector/

    
