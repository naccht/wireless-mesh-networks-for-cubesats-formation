#!/usr/bin/python

import serial
import time
from xbee import XBee, ZigBee
from digimesh import DigiMesh

serial_port = serial.Serial('/dev/ttyUSB0', 9600)

def print_data(data):
    """
    This method is called whenever data is received
    from the associated XBee device. Its first and
    only argument is the data contained within the
    frame.
    """
    print "I got called : and I see"
    print data

#xbee = XBee(serial_port, callback=print_data)
#xbee = ZigBee(serial_port)
xbee = DigiMesh(serial_port)

response = "init"
while True:
    try:
        print "listening"
#        xbee.send('at', frame_id='A', command='DH')
        response = xbee.wait_read_frame()
        print response
    except KeyboardInterrupt:
        break
#    except:
#        print "err"
#        print response
#        continue

xbee.halt()
serial_port.close()